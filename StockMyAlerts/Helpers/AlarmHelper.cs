﻿
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;

namespace StockMyAlerts.Helpers
{
    public static class AlarmHelper
    {
       
        public static void StartAlarms(Context context)
        {
            //kick off our background process to check stock alerts
            var alarmIntent = new Intent(context, typeof(BackgroundDataReceiver));

            //if we already have an alarm scheduled..we dont need to reschedule, which will throw the timing off
            var existingIntent = PendingIntent.GetBroadcast(context, 0, alarmIntent, PendingIntentFlags.NoCreate);
            if (existingIntent == null)
            {
                var pending = PendingIntent.GetBroadcast(context, 0, alarmIntent, PendingIntentFlags.UpdateCurrent);
                var alarmManager = context.GetSystemService("alarm").JavaCast<AlarmManager>();
                alarmManager.SetExactAndAllowWhileIdle(AlarmType.ElapsedRealtimeWakeup, SystemClock.ElapsedRealtime() + 30000, pending); //start the background data receiver 
            }
        }

        public static void SetNextAlarm(Context context,int checkInterval)
        {
            var alarmIntent = new Intent(context, typeof(BackgroundDataReceiver));

            var pending = PendingIntent.GetBroadcast(context, 0, alarmIntent, PendingIntentFlags.UpdateCurrent);
            var alarmManager = context.GetSystemService("alarm").JavaCast<AlarmManager>();
            alarmManager.SetExactAndAllowWhileIdle(AlarmType.ElapsedRealtimeWakeup, SystemClock.ElapsedRealtime() + checkInterval, pending); //start the background data receiver 

        }

        public static void StopAlarms(Context context)
        {
            //kick off our background process to check stock alerts
            var alarmIntent = new Intent(context, typeof(BackgroundDataReceiver));
            var pending = PendingIntent.GetBroadcast(context, 0, alarmIntent, PendingIntentFlags.UpdateCurrent);
            var alarmManager = context.GetSystemService("alarm").JavaCast<AlarmManager>();
            if (pending != null)
            {
                alarmManager.Cancel(pending);
                pending.Cancel();
            }
            
        }



    }
}