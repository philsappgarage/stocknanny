﻿using Microsoft.AppCenter.Analytics;
using Newtonsoft.Json;
using RestSharp;
using StockMyAlerts.Entities;
using System;
using System.Collections.Generic;

namespace StockMyAlerts
{
    public static class RestData
    {
        static string apiToken = "pk_c159bfedd27e4a428bb79da1ddd1d426";

        public static decimal GetCurrentStockPrice(string symbol)
        {
            string reqUri = string.Format("https://cloud.iexapis.com/stable/stock/{0}/price?token={1}", symbol, apiToken);
            RestClient client = new RestClient(reqUri);
            RestRequest request = new RestRequest(Method.GET);
            IRestResponse response = client.Execute(request);
            if(decimal.TryParse(response.Content,out decimal currentPrice))
            {
                return currentPrice;
            }
            else
            {
                
                Analytics.TrackEvent("GetCurrentStockPrice Error " + DateTime.Now.ToShortDateString());
                return -1;
            }
            
        }

        public static StockQuote GetStockQuote(string symbol)
        {
            string reqUri = string.Format("https://cloud.iexapis.com/stable/stock/{0}/quote?token={1}", symbol, apiToken);
            RestClient client = new RestClient(reqUri);
            RestRequest request = new RestRequest(Method.GET);
            IRestResponse<StockQuote> response = client.Execute<StockQuote>(request);

            return response.Data;
        }

        public static List<string> Get100Symbols()
        {
            List<string> Symbols = new List<string>();
            Symbols.Add("NFLX");
            Symbols.Add("SBUX");
            Symbols.Add("FNSR");
            Symbols.Add("FE");
            Symbols.Add("MRK");
            Symbols.Add("COLD");
            Symbols.Add("JNPR");
            Symbols.Add("MSFT");
            Symbols.Add("ABT");
            Symbols.Add("AMZN");
            Symbols.Add("ACN");
            Symbols.Add("XOM");
            Symbols.Add("X");
            Symbols.Add("AXP");
            Symbols.Add("ADBE");
            Symbols.Add("AVP");
            Symbols.Add("AGN");
            Symbols.Add("ANF");
            Symbols.Add("ABBV");
            Symbols.Add("CAG");
            Symbols.Add("AEE");
            Symbols.Add("MMM");
            Symbols.Add("AZO");
            Symbols.Add("AVY");
            Symbols.Add("APD");
            Symbols.Add("ALL");
            Symbols.Add("AKAM");
            Symbols.Add("ARW");
            Symbols.Add("GLD");
            Symbols.Add("INTC");
            Symbols.Add("CELG");
            Symbols.Add("CMCSA");
            Symbols.Add("NKE");
            Symbols.Add("CSCO");
            Symbols.Add("AMGN");
            Symbols.Add("VZ");
            Symbols.Add("MRO");
            Symbols.Add("AAN");
            Symbols.Add("AYI");
            Symbols.Add("AHC");
            Symbols.Add("AKS");
            Symbols.Add("TXN");
            Symbols.Add("ADSK");
            Symbols.Add("APH");
            Symbols.Add("AIV");
            Symbols.Add("TGT");
            Symbols.Add("AVB");
            Symbols.Add("ADM");
            Symbols.Add("AEM");
            Symbols.Add("AUY");
            Symbols.Add("PAYX");
            Symbols.Add("ABX");
            Symbols.Add("DRI");
            Symbols.Add("AMAT");
            Symbols.Add("AFG");
            Symbols.Add("ALTR");
            Symbols.Add("AIZ");
            Symbols.Add("DHI");
            Symbols.Add("DJCO");
            Symbols.Add("DAKT");
            Symbols.Add("DAN");
            Symbols.Add("DHR");
            Symbols.Add("DAC");
            Symbols.Add("DQ");
            Symbols.Add("DRI");
            Symbols.Add("DARE");
            Symbols.Add("DRIO");
            Symbols.Add("DAIO");
            Symbols.Add("DAR");
            Symbols.Add("DZSI");
            Symbols.Add("DSKE");
            Symbols.Add("DTSS");
            Symbols.Add("PLAY");
            Symbols.Add("DTEA");
            Symbols.Add("DFNL");
            Symbols.Add("DINT");
            Symbols.Add("DUSA");
            Symbols.Add("DWLD");
            Symbols.Add("DVA");
            Symbols.Add("DWSN");
            Symbols.Add("DXR");
            Symbols.Add("DBVT");
            Symbols.Add("DCP");
            Symbols.Add("DDMX");
            Symbols.Add("DF");
            Symbols.Add("DCPH");
            Symbols.Add("DECK");
            Symbols.Add("DE");
            Symbols.Add("DFRG");
            Symbols.Add("TACO");
            Symbols.Add("DEX");
            Symbols.Add("VCF");
            Symbols.Add("DDF");
            Symbols.Add("VFL");
            Symbols.Add("VMM");
            Symbols.Add("DKL");
            Symbols.Add("DK");
            Symbols.Add("DELL");
            Symbols.Add("DMPI");
            Symbols.Add("NOK");

            return Symbols;

        }
        public static List<StockQuote> GetStockQuotesBatch(List<string> symbols)
        {
            List<StockQuote> quotes = new List<StockQuote>();

            if(symbols.Count == 0 || symbols == null)
            {
                return quotes;
            }

            string reqUri = string.Format("https://cloud.iexapis.com/stable/stock/market/batch?symbols={0}&types=quote&token={1}", string.Join(",",symbols),apiToken);
            RestClient client = new RestClient(reqUri);
            RestRequest request = new RestRequest(Method.GET);
            var response = client.Execute(request);
           
            var dict = JsonConvert.DeserializeObject<Dictionary<string,StockQuoteContainer>>(response.Content); //json returned is a key/value pair format
            foreach(KeyValuePair<string,StockQuoteContainer> entry in dict) //loop through and populate our list of quotes
            { 
                quotes.Add(entry.Value.Quote);
            }

            return quotes;
        }

        public static List<StockPrice> GetStockPricesBatch(List<string> symbols)
        {
            List<StockPrice> prices = new List<StockPrice>();

            if(symbols.Count == 0 || symbols == null )
            {
                return prices;
            }

            string reqUri = string.Format("https://cloud.iexapis.com/stable/stock/market/batch?symbols={0}&types=price&token={1}", string.Join(",", symbols),apiToken);

         
            RestClient client = new RestClient(reqUri);
            RestRequest request = new RestRequest(Method.GET);
            var response = client.Execute(request);

            var dict = JsonConvert.DeserializeObject<Dictionary<string, StockPriceContainer>>(response.Content); //json returned is a key/value pair format
            foreach (KeyValuePair<string, StockPriceContainer> entry in dict) //loop through and populate our list of quotes
            {
                StockPrice price = new StockPrice();
                price.Symbol = entry.Key;
                price.Price = entry.Value.Price;
                prices.Add(price);
            }

            return prices;
        }

        private class CompanyInfoResponse
        {
            public string symbol { get; set; }
            public string companyName { get; set; }

        }

       
        public static string GetCompanyName(string symbol)
        {
            string reqUri = string.Format("https://cloud.iexapis.com/stable/stock/{0}/company?token={1}", symbol,apiToken);
            RestClient client = new RestClient(reqUri);
            RestRequest request = new RestRequest(Method.GET);
            IRestResponse<CompanyInfoResponse> response = client.Execute<CompanyInfoResponse>(request);
            
            return response.Data.companyName;
        }


    }
}