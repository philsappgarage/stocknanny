﻿using SQLite;
using System;

namespace StockMyAlerts
{
    public class StockAlert
    {
        [PrimaryKey,AutoIncrement]
        public int Id { get; set; }
        public string Ticker { get; set;}
        public string CompanyName { get; set; }
        public decimal AlertPrice { get; set; }
        public decimal CurrentPrice { get; set; }
        public string CompareOperator { get; set; }
        public string AlertMessage { get; set; }
        public bool IncludeExtendedHours { get; set; }
        public bool AlertEnabled { get; set; }
        public int AlertCount { get; set; }
        public DateTime?  LastNotification { get; set; }
    }
    
}