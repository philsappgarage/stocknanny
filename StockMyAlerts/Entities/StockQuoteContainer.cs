﻿/// <summary>
/// This is a container class that is used in the batch api calls that is needed based on the returned JSON data
/// </summary>
namespace StockMyAlerts
{
    public class StockQuoteContainer
    {
        public StockQuote Quote { get; set; }
    }
}