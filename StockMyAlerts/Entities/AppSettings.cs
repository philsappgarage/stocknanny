﻿namespace StockMyAlerts
{
    public class AppSettings
    {
        public int Id { get; set; }
        public bool PauseAlerts { get; set; }
        public int PriceCheckInterval { get; set; }
        public int SnoozeInterval { get; set; }
        public int MaxAlertCount { get; set; }
        public bool MonitorExtendedHours { get; set; }
    }
}