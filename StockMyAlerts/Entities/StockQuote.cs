﻿namespace StockMyAlerts
{
    public class StockQuote
    {
        public string Symbol { get; set; }
        public string CompanyName { get; set; }
        public decimal LatestPrice { get; set; }
        public decimal Change { get; set; }
        public decimal ChangePercent { get; set; }
        public long AvgTotalVolume { get; set;}
        public long LatestVolume { get; set; }
        public decimal Week52High { get; set; }
        public decimal Week52Low { get; set; }
      
    }
}