﻿namespace StockMyAlerts
{
    public class StockPrice
    {
        public string Symbol { get; set; }
        public decimal Price { get; set; }
    }
}