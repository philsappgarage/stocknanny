﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace StockMyAlerts
{
    public class StockListAdapter : BaseAdapter<StockAlert>
    {
        List<StockAlert> items;
        Activity context;

        public StockListAdapter(Activity context, List<StockAlert> items)
      : base()
        {
            this.context = context;
            this.items = items;
        }
        public override long GetItemId(int position)
        {
            return position;
        }
        public override StockAlert this[int position]
        {
            get { return items[position]; }
        }
        public override int Count
        {
            get { return items.Count; }
        }
        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var item = items[position];
            View view = convertView;
            if (view == null) // no view to re-use, create new
                view = context.LayoutInflater.Inflate(Resource.Layout.StockListRow, null);
            view.FindViewById<TextView>(Resource.Id.rowSymbol).Text = item.Ticker;
            view.FindViewById<TextView>(Resource.Id.rowCurrentPrice).Text = "$" + item.CurrentPrice.ToString("F");
            string symbol = item.CompareOperator.ToLower().Contains("exceeds") ? ">" : "<";
            view.FindViewById<TextView>(Resource.Id.rowAlertPrice).Text = symbol + " $" + item.AlertPrice.ToString("F");
            view.FindViewById<TextView>(Resource.Id.rowCompany).Text = item.CompanyName;
            
            return view;
        }


    }
}