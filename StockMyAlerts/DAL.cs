﻿using Android.Util;
using SQLite;
using System;
using System.Collections.Generic;


namespace StockMyAlerts
{
    public class DAL
    {

        string folder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);

        public DAL()
        {
            createDatabase();
        }


        public bool createDatabase()
        {
            try
            {
                using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, "StockAlerts.db")))
                {
                    connection.CreateTable<StockAlert>();
                    connection.CreateTable<AppSettings>();
                    return true;
                }
            }
            catch (SQLiteException ex)
            {
                Log.Info("SQLiteEx", ex.Message);
                return false;
            }
        }

        #region StockAlerts
        public bool insertAlert(StockAlert alert)
        {
            try
            {
                using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, "StockAlerts.db")))
                {
                    connection.Insert(alert);
                    return true;
                }
            }
            catch (SQLiteException ex)
            {
                Log.Info("SQLiteEx", ex.Message);
                return false;
            }
        }

        public StockAlert getAlertById(int id)
        {
            try
            {
                using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, "StockAlerts.db")))
                {
                    return connection.Get<StockAlert>(id);
                }
            }
            catch (SQLiteException ex)
            {
                Log.Info("SQLiteEx", ex.Message);
                return null;
            }
        }

        public List<StockAlert> getAllAlerts()
        {
            try
            {
                using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, "StockAlerts.db")))
                {
                    return connection.Table<StockAlert>().OrderBy(x=> x.Ticker).ToList();
                }
            }
            catch (SQLiteException ex)
            {
                Log.Info("SQLiteEx", ex.Message);
                return null;
            }
        }

        public bool resetAlertAfterMaxNotifications(StockAlert alert)
        {
            try
            {
                using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, "StockAlerts.db")))
                {
                    connection.Query<StockAlert>("UPDATE StockAlert set AlertEnabled=?, AlertCount=?,LastNotification=? Where Id=?", false,0,DateTime.Now, alert.Id);
                    return true;
                }
            }
            catch (SQLiteException ex)
            {
                Log.Info("SQLiteEx", ex.Message);
                return false;
            }
        }
        
        public bool updateAlert(StockAlert alert)
        {
            try
            {
                using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, "StockAlerts.db")))
                {
                    connection.Query<StockAlert>("UPDATE StockAlert set Ticker=?, AlertPrice=?, CompareOperator=?, " +
                        "AlertMessage=?,AlertEnabled=?,CompanyName=? Where Id=?", alert.Ticker, alert.AlertPrice, alert.CompareOperator, 
                        alert.AlertMessage,alert.AlertEnabled,alert.CompanyName,alert.Id);
                    return true;
                }
            }
            catch (SQLiteException ex)
            {
                Log.Info("SQLiteEx", ex.Message);
                return false;
            }
        }

        public bool updateAlertNotificationData(StockAlert alert)
        {
            int alertCount = alert.AlertCount + 1;

            try
            {
                using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, "StockAlerts.db")))
                {
                    connection.Query<StockAlert>("UPDATE StockAlert set AlertCount=?,LastNotification=? Where Id=?", alertCount,DateTime.Now, alert.Id);
                    return true;
                }
            }
            catch (SQLiteException ex)
            {
                Log.Info("SQLiteEx", ex.Message);
                return false;
            }
        }

        public bool deleteStockAlert(StockAlert alert)
        {
            try
            {
                using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, "StockAlerts.db")))
                {
                    connection.Delete(alert);
                    return true;
                }
            }
            catch (SQLiteException ex)
            {
                Log.Info("SQLiteEx", ex.Message);
                return false;
            }
        }
        #endregion

        #region AppSettings

        public void InitAppSettings()
        {
            try
            {
                using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, "StockAlerts.db")))

                    if (connection.Table<AppSettings> ().Count() == 0) //we dont have our appsettings row setup in the table yet, lets do it now
                    {
                        var appSettings = new AppSettings();
                        appSettings.Id = 1;
                        appSettings.MaxAlertCount = 10;
                        appSettings.PauseAlerts = false;
                        appSettings.PriceCheckInterval = 60;
                        appSettings.SnoozeInterval = 10;
                        appSettings.MonitorExtendedHours = false;

                        connection.Insert(appSettings);
                    }

            }
            catch (SQLiteException ex)
            {
                Log.Info("SQLiteEx", ex.Message);
            }
        }

        public bool UpdatePauseAlerts(bool pauseAlerts)
        {
            try
            {
                using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, "StockAlerts.db")))
                {
                    connection.Query<AppSettings>("UPDATE AppSettings set PauseAlerts=? where id=?", pauseAlerts, 1);
                    return true;
                }
            }
            catch (SQLiteException ex)
            {
                Log.Info("SQLiteEx", ex.Message);
                return false;
            }
        }

    

        public AppSettings GetAppSettings()
        {
            try
            {
                using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, "StockAlerts.db")))
                {
                    return connection.Get<AppSettings>(1);
                }
            }
            catch (SQLiteException ex)
            {
                Log.Info("SQLiteEx", ex.Message);
                return null;
            }
        }

        public bool UpdateAppSettings(AppSettings appSettings)
        {
            try
            {
                using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, "StockAlerts.db")))
                {
                    connection.Query<AppSettings>("UPDATE AppSettings set PauseAlerts=?,PriceCheckInterval=?,SnoozeInterval=?," +
                        "MaxAlertCount=?,MonitorExtendedHours=? where Id=1", 
                        appSettings.PauseAlerts,appSettings.PriceCheckInterval,appSettings.SnoozeInterval,appSettings.MaxAlertCount,appSettings.MonitorExtendedHours);
                    return true;
                }
            }
            catch (SQLiteException ex)
            {
                Log.Info("SQLiteEx", ex.Message);
                return false;
            }
        }

        #endregion

    }








}
