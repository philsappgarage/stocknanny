﻿
using Android.App;
using Android.Content;
using Firebase.JobDispatcher;
using StockMyAlerts.Services;

namespace StockMyAlerts
{
    [BroadcastReceiver]
    [IntentFilter(new[] { Android.Content.Intent.ActionBootCompleted })]
    public class BootReceiver : BroadcastReceiver
    {
        public override void OnReceive(Context context, Intent intent)
        {
            //this receiver should fire if the device is rebooted...we need to restart our background data receiver to check the stocks
              var bgDataReceiverIntent = new Intent(context, typeof(BackgroundDataReceiver));
              bgDataReceiverIntent.PutExtra("Reboot", 1);
              context.SendBroadcast(bgDataReceiverIntent);

           /* FirebaseJobDispatcher dispatcher = context.CreateJobDispatcher();

            Job myJob = dispatcher.NewJobBuilder()
                                  .SetService<BackgroundDataService>("bg-data-service")
                                  .Build();
            int scheduleResult = dispatcher.Schedule(myJob);*/


        }

    }
}