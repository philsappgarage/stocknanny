﻿using Android.App;
using Firebase.JobDispatcher;
using System;
using Android.Content;
using Android.Media;
using Android.OS;
using Android.Support.V4.App;
using System.Collections.Generic;
using System.Linq;
using TaskStackBuilder = Android.Support.V4.App.TaskStackBuilder;
using System.Threading.Tasks;

namespace StockMyAlerts.Services
{
    [Service(Name = "StockNanny.BackgroundData")]
    [IntentFilter(new[] { FirebaseJobServiceIntent.Action })]
    public class BackgroundDataService : Firebase.JobDispatcher.JobService
    {
        //for debugging
        bool ignoreTime = true;

        //for notifications
        static readonly string CHANNEL_ID = "location_notification";
        internal static readonly string COUNT_KEY = "count";

        private DAL dal;
        private List<StockAlert> listAlerts;
        private AppSettings appSettings;

        private DateTime marketOpen = DateTime.Parse("2012/12/12 09:30:00.000"); //9:30am EST or 2:30pm UTC
        private DateTime marketClose = DateTime.Parse("2012/12/12 16:00:00.000"); //4pm EST or 9pm UTC
        private DateTime marketOpenExtended = DateTime.Parse("2012/12/12 06:00:00.000"); //6:00am EST or 11am UTC if they chose extended hours
        private DateTime marketCloseExtended = DateTime.Parse("2012/12/12 20:00:00.000"); //8pm EST or 1am UTC if they chose extended hours
        private DayOfWeek[] marketDays = { DayOfWeek.Monday, DayOfWeek.Tuesday, DayOfWeek.Wednesday, DayOfWeek.Thursday, DayOfWeek.Friday };
        private bool isValidMarketTime = false;
        private int msBeforeMarketOpen = 0; //milliseconds before market open


        public override bool OnStartJob(IJobParameters jobParameters)
        {
            Task.Run(async () =>
            {
                int checkInterval = await RunProcessing();

                //schedule our next job
                ScheduleNextJob(checkInterval);

                //call our current job finished for garbage collection
                JobFinished(jobParameters, false);
            });


            return true;
        }

        private void ScheduleNextJob(int checkInterval)
        {
            FirebaseJobDispatcher dispatcher = this.CreateJobDispatcher();
            checkInterval = checkInterval / 1000; // job scheduler actually uses sec instead of ms 

            JobTrigger myTrigger = Trigger.ExecutionWindow(checkInterval,checkInterval + 5); //execute between check interval, and 5 seconds from check interval
            RetryStrategy retry = dispatcher.NewRetryStrategy(RetryStrategy.RetryPolicyLinear, 60, 300); //if the job fails to start keep retrying from 1 min - 5 mins
            Job myJob = dispatcher.NewJobBuilder()
                                  .SetService<BackgroundDataService>("bg-data-service")
                                  .SetTrigger(myTrigger)
                                  .SetRetryStrategy(retry)
                                  .Build();
            int scheduleResult = dispatcher.Schedule(myJob);

        }

        public override bool OnStopJob(IJobParameters jobParameters)
        {
            return false;
        }

        public void InitSettings()
        {
            dal = new DAL();
            appSettings = dal.GetAppSettings();
            CheckValidMarketTime();
        }

        async Task<int> RunProcessing()
        {
            InitSettings(); //make sure we instantiate our dal and our appsettings

            if (isValidMarketTime && !appSettings.PauseAlerts)
            {
                CreateNotificationChannel();
                listAlerts = dal.getAllAlerts(); //get our list of alerts
                CheckStockPrices();

            }

          
            int checkInterval = 60000; //we will set our alarm for every minute during off hours
            if (msBeforeMarketOpen > 0)
            {
                checkInterval = msBeforeMarketOpen;
            }


            if (isValidMarketTime) //set the check interval to the user settings
            {
                checkInterval = appSettings.PriceCheckInterval * 1000; // we need this in ms
                if (checkInterval < 20000)
                {
                    checkInterval = 20000; //we only want to support a minimum check interval of 20 seconds..we protect this on the front end, but we're being extra cautious
                }
            }

            return appSettings.PriceCheckInterval * 1000;


        }

        public void CheckValidMarketTime()
        {
            //we should only check the stock price during the appropriate hours
            //we are getting the UTC time, and then converting it to EST..bc that makes comparing the times easier..
            //if we compared in UTC..then extended closed time because 1am the next day..which makes time comparison trickier
            DateTime currentTimeUTC = DateTime.UtcNow;
            DateTime currentDate = DateTime.Now;
            TimeZoneInfo estInfo = TimeZoneInfo.FindSystemTimeZoneById("America/New_York");
            DateTime currentTimeEST = TimeZoneInfo.ConvertTimeFromUtc(currentTimeUTC, estInfo);

            DateTime openTime = appSettings.MonitorExtendedHours ? marketOpenExtended : marketOpen;
            DateTime closeTime = appSettings.MonitorExtendedHours ? marketCloseExtended : marketClose;

            if ((currentTimeEST.TimeOfDay >= openTime.TimeOfDay &&
             currentTimeEST.TimeOfDay <= closeTime.TimeOfDay &&
             marketDays.Contains(currentDate.DayOfWeek)) || ignoreTime)
            {
                isValidMarketTime = true;
            }
            else
            {
                isValidMarketTime = false;
            }

            //check if we are past 8pm..then calculate the number of ms until extended market open..and set that value for use in the next alarm
            if (currentTimeEST.TimeOfDay > marketCloseExtended.TimeOfDay || currentTimeEST.TimeOfDay < marketOpenExtended.TimeOfDay)
            {
                TimeSpan nextMarketOpen = marketOpenExtended.TimeOfDay - currentTimeEST.TimeOfDay;
                if (nextMarketOpen.TotalMilliseconds > 0)
                {
                    msBeforeMarketOpen = (int)nextMarketOpen.TotalMilliseconds;
                }
                else
                {
                    nextMarketOpen += TimeSpan.FromHours(24);
                    msBeforeMarketOpen = (int)nextMarketOpen.TotalMilliseconds;
                }
            }
        }


        public void CheckStockPrices()
        {
            if (listAlerts.FindAll(x => x.AlertEnabled).Count < 1) //if we dont have any alerts..no need to keep doing anything
            {
                return;
            }

            //get our list of quotes from our enabled alert Symbols
            List<StockQuote> quotes = RestData.GetStockQuotesBatch(listAlerts.Where(x => x.AlertEnabled).Select(x => x.Ticker).ToList());

            foreach (StockAlert alert in listAlerts.Where(x => x.AlertEnabled))
            {

                decimal currentPrice = quotes.Find(x => x.Symbol == alert.Ticker).LatestPrice;

                if (alert.CompareOperator.ToLower().Contains("exceeds"))
                {
                    if (currentPrice >= alert.AlertPrice)
                    {
                        if (CheckNotificationSettings(alert))
                        {
                            SendNotification(alert);
                            dal.updateAlertNotificationData(alert);
                        }
                    }
                }

                else
                {
                    if (currentPrice <= alert.AlertPrice)
                    {
                        if (CheckNotificationSettings(alert))
                        {
                            SendNotification(alert);
                            dal.updateAlertNotificationData(alert);
                        }
                    }
                }

            }

        }

        bool CheckNotificationSettings(StockAlert alert)
        {

            int currentAlertCount = alert.AlertCount + 1;

            if (currentAlertCount > appSettings.MaxAlertCount)
            {
                //disable the alert , reset counter, and update alert time
                dal.resetAlertAfterMaxNotifications(alert);
                return false;
            }

            //check our snooze settings
            if (alert.LastNotification != null)
            {
                var nextValidAlertTime = alert.LastNotification.Value.AddMinutes(appSettings.SnoozeInterval);
                if (nextValidAlertTime > DateTime.Now)
                {
                    return false;
                }
            }

            return true;
        }



        void CreateNotificationChannel()
        {
            if (Build.VERSION.SdkInt < BuildVersionCodes.Eclair)
            {
                // Notification channels are new in API 26 (and not a part of the
                // support library). There is no need to create a notification
                // channel on older versions of Android.
                return;
            }

            var channelName = this.Resources.GetString(Resource.String.channel_name);
            var channelDescription = this.GetString(Resource.String.channel_description);
            var channel = new NotificationChannel(CHANNEL_ID, channelName, NotificationImportance.Default)
            {
                Description = channelDescription
            };

            var notificationManager = (NotificationManager)this.GetSystemService(Context.NotificationService);

            notificationManager.CreateNotificationChannel(channel);
        }

        void SendNotification(StockAlert alert)
        {
            //set up our activity/intent for when the user clicks on the notification, we want to bring them to the alerts properties page
            var alertIntent = new Intent(this, typeof(EditAlertActivity));
            alertIntent.PutExtra("StockAlertId", alert.Id);
            alertIntent.PutExtra("FromNotification", 1);
            // Construct a back stack for cross-task navigation:
            var stackBuilder = TaskStackBuilder.Create(this);
            stackBuilder.AddParentStack(Java.Lang.Class.FromType(typeof(EditAlertActivity)));
            stackBuilder.AddNextIntent(alertIntent);

            //generate a unique id for this notification
            int uniqueId = (int)((DateTime.Now.TimeOfDay.Ticks / 1000L) % int.MaxValue);

            var pendingIntent = stackBuilder.GetPendingIntent(uniqueId, (int)PendingIntentFlags.UpdateCurrent);

            // Instantiate the builder and set notification elements:
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .SetSound(RingtoneManager.GetDefaultUri(RingtoneType.Notification))
                .SetAutoCancel(true)
                .SetContentIntent(pendingIntent)
                .SetContentTitle("Stock Alert - " + alert.Ticker)
                .SetContentText(alert.AlertMessage)
                .SetSmallIcon(Resource.Drawable.granny_logo);

            // Build the notification:
            Notification notification = builder.Build();

            // Get the notification manager:
            NotificationManager notificationManager =
                this.GetSystemService(Context.NotificationService) as NotificationManager;

            // Publish the notification - we need a unique notification id if we want to be able to send multiple notifications
            notificationManager.Notify(uniqueId, notification);
        }


    }
}