﻿
using Android.App;
using Android.Content;
using Android.OS;
using Android.Support.V7.App;
using System;
using System.Threading.Tasks;

namespace StockMyAlerts.Activities
{
    [Activity(Theme = "@style/SplashTheme", MainLauncher = true, NoHistory = true)]
    public class SplashActivity : AppCompatActivity
    {
        
        public override void OnCreate(Bundle savedInstanceState, PersistableBundle persistentState)
        {
            base.OnCreate(savedInstanceState, persistentState);
            
        }

        // Launches the startup task
        protected override void OnResume()
        {
            base.OnResume();
            Task startupWork = new Task(() => { BootstrapData(); });
            startupWork.Start();
        }

        // make some bunk rest calls which will setup our REST connection..and everything will run faster
         void BootstrapData()
        {
            try
            {
                decimal val = RestData.GetCurrentStockPrice("GOOG");
                StockQuote quote = RestData.GetStockQuote("AAPL");
            }
            catch (Exception ex)
            {
                //do nothing, let it continue 
            }
            
            StartActivity(new Intent(Application.Context, typeof(MainActivity)));
        }

        public override void OnBackPressed() { }
    }
}