﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;
using System;

namespace StockMyAlerts
{
    [Activity(Label = "Stock Nanny")]
    public class EditAlertActivity : Activity
    {
        private DAL dal;
        private StockAlert selectedAlert;
        private int maxAlertsAllowed = 2;
        private int currentAlertCount;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            dal = new DAL();
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.EditAlert);

            //set up our spinner
            Spinner spinner = FindViewById<Spinner>(Resource.Id.lstCompareOperator);
            var adapter = ArrayAdapter.CreateFromResource(this, Resource.Array.alert_conditions, Android.Resource.Layout.SimpleSpinnerItem);
            adapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
            spinner.Adapter = adapter;

            //hide our Toolbar spinner..we dont need it for this page
            Spinner alertSpinner = FindViewById<Spinner>(Resource.Id.alert_view_spinner);
            alertSpinner.Visibility = ViewStates.Gone;

            //setup our buttons

            Button btnAdd = FindViewById<Button>(Resource.Id.btnSaveAlert);
           
            Button btnFind = FindViewById<Button>(Resource.Id.btnSymbolSearch);

            btnAdd.Click += delegate {
                SaveAlert(); 
            };

            btnFind.Click += delegate {
                FindStockInfo(); 
            };

            //get our current number of alerts in the system, passed in from the main page so we dont need to go back to the db
            currentAlertCount = Intent.GetIntExtra("currentAlertCount", 0);

            //check to see if this was from an incoming notification
            bool fromNotification = Intent.GetIntExtra("FromNotification", 0) == 1 ? true : false;
            
            //if this is an edit, we need to get and set our selectedAlert
            int alertId = Intent.GetIntExtra("StockAlertId",-1);
            if(alertId != -1)
            {
                SetSelectedAlert(alertId);
                btnFind.Visibility = ViewStates.Gone; //hide the find button if its an edit..they should just create a new alert to change the ticker
                EditText symbolTxt = FindViewById<EditText>(Resource.Id.txtSymbol);//also make the ticker not editable
                symbolTxt.Enabled = false;
            }
            
           
            //setup our toolbar
            var toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
            SetActionBar(toolbar);
            ActionBar.Title = "Alert Details";

        }

        private void FindStockInfo()
        {
            ClearStockInfo(); 
            string symbol = FindViewById<EditText>(Resource.Id.txtSymbol).Text;

            StockQuote quote = RestData.GetStockQuote(symbol);

            if(quote != null)
            {
                //populate our stock data
                FindViewById<TextView>(Resource.Id.txtCompanyName).Text = quote.CompanyName;
                FindViewById<TextView>(Resource.Id.txtCurrentPrice).Text = "Current Price: $" + quote.LatestPrice.ToString("F");
                quote.ChangePercent = Math.Round((quote.ChangePercent * 100), 2); //round our change percentage to 2 places
                FindViewById<TextView>(Resource.Id.txtCurrentPercent).Text = "Change Percentage: " + quote.ChangePercent.ToString() + "%";
                decimal milVol = quote.LatestVolume / 1000000m; 
                decimal rndMilVol = Math.Round(milVol, 1);
                FindViewById<TextView>(Resource.Id.txtCurrentVolume).Text = "Current Volume(M): " + rndMilVol.ToString();
                milVol = quote.AvgTotalVolume / 1000000m;
                rndMilVol = Math.Round(milVol, 1);
                FindViewById<TextView>(Resource.Id.txtAverageVolume).Text = "Average Volume(M): " + rndMilVol.ToString();
                //round our highs and lows to two decimal places
                FindViewById<TextView>(Resource.Id.txt52High).Text = "52 Week High: $" + quote.Week52High.ToString("F");
                FindViewById<TextView>(Resource.Id.txt52Low).Text = "52 Week Low: $" + quote.Week52Low.ToString("F");
            }
            else
            {
                string toastMsg = string.Format("Symbol: {0} is not valid ", symbol);
                Toast toast = Toast.MakeText(this, toastMsg, ToastLength.Short);
                toast.Show();
            }

        }


        private void ClearStockInfo()
        {
            FindViewById<TextView>(Resource.Id.txtCompanyName).Text = "";
            FindViewById<TextView>(Resource.Id.txtCurrentPrice).Text = "Current Price:";
            FindViewById<TextView>(Resource.Id.txtCurrentPercent).Text = "Change Percantage:";
            FindViewById<TextView>(Resource.Id.txtCurrentVolume).Text = "Current Volume:";
            FindViewById<TextView>(Resource.Id.txtAverageVolume).Text = "Average Volume:";
            FindViewById<TextView>(Resource.Id.txt52High).Text = "52 Week High:";
            FindViewById<TextView>(Resource.Id.txt52Low).Text = "52 Week Low:";
        }

        private bool VerifyData()
        {
            //verify the Symbol is valid
            string tickerSymbol = FindViewById<EditText>(Resource.Id.txtSymbol).Text;
            if (RestData.GetCurrentStockPrice(tickerSymbol) == -1)
            {
                string toastMsg = string.Format("Symbol: {0} is not valid ", tickerSymbol);
                Toast toast = Toast.MakeText(this, toastMsg, ToastLength.Short);
                toast.Show();
                return false;

            }

            //verify the price is valid
            if (decimal.TryParse(FindViewById<EditText>(Resource.Id.txtAlertPrice).Text, out decimal alertPrice))
            {
                if (decimal.Round(alertPrice, 2) != alertPrice) // 2 decimal places only!
                {
                    string toastMsg = ("Alert Price Is Not Valid");
                    Toast toast = Toast.MakeText(this, toastMsg, ToastLength.Short);
                    toast.Show();
                    return false;

                }
            }
            else
            {
                string toastMsg = ("Alert Price Is Not Valid");
                Toast toast = Toast.MakeText(this, toastMsg, ToastLength.Short);
                toast.Show();
                return false;
            }


            return true;
        }

        private void SetSelectedAlert(int alertId)
        {
            //get the alert
            StockAlert alert = dal.getAlertById(alertId);
            this.selectedAlert = alert;

            //populate our fields
            FindViewById<EditText>(Resource.Id.txtSymbol).Text = alert.Ticker;
            FindViewById<EditText>(Resource.Id.txtAlertPrice).Text = alert.AlertPrice.ToString();
            int valPos = alert.CompareOperator.Contains("Exceeds") ? 0 : 1;
            FindViewById<Spinner>(Resource.Id.lstCompareOperator).SetSelection(valPos);
            FindViewById<EditText>(Resource.Id.txtAlertMessage).Text = alert.AlertMessage ?? "";
            FindViewById<Switch>(Resource.Id.switchEnabled).Checked = alert.AlertEnabled;

            //set the current stock info
            FindStockInfo();
        }

        private void DeleteAlert()
        {
            //tell them to pound salt if the alert is new
            if (selectedAlert == null)
            {
                string msg = "Alert Has Not Yet Been Saved";
                Toast toastInfo = Toast.MakeText(this, msg, ToastLength.Short);
                toastInfo.Show();
                return;
            }

            dal.deleteStockAlert(selectedAlert);
            string toastMsg = string.Format("{0} Alert Deleted", selectedAlert.Ticker);
            Toast toast = Toast.MakeText(this, toastMsg, ToastLength.Short);
            toast.Show(); //give em a message telling them it was deleted

            //navigate back to the main page
            var intent = new Intent(this, typeof(MainActivity));
            this.StartActivity(intent);
        }

        private void SaveAlert()
        {
            StockAlert alert = selectedAlert ?? new StockAlert(); //use our selected alert if we have one..otherwise create a new one 

            //if they are adding a new alert..we need to make sure they arent exceeding the max alert count
            if(alert.Id == 0)
            {
                if(currentAlertCount + 1 > maxAlertsAllowed)
                {
                    TellUserToPoundSalt();
                    return;
                }
            }


            if (VerifyData())
            {
                //set all the fields in our alert object
                alert.Ticker = FindViewById<EditText>(Resource.Id.txtSymbol).Text;
                alert.AlertPrice = decimal.Parse(FindViewById<EditText>(Resource.Id.txtAlertPrice).Text);
                var spinner = FindViewById<Spinner>(Resource.Id.lstCompareOperator);
                alert.CompareOperator = spinner.GetItemAtPosition(spinner.SelectedItemPosition).ToString();
                alert.AlertMessage = FindViewById<EditText>(Resource.Id.txtAlertMessage).Text;
                Switch s2 = FindViewById<Switch>(Resource.Id.switchEnabled);
                alert.AlertEnabled = s2.Checked;

                //get and set the company name
                alert.CompanyName = RestData.GetCompanyName(alert.Ticker);

                if (alert.Id == 0) //we're creating a new one
                {
                    alert.AlertCount = 0;
                    dal.insertAlert(alert);
                }
                else
                {
                    dal.updateAlert(alert);
                }

                string toastMsg = string.Format("{0} Alert Saved", alert.Ticker);
                Toast toast = Toast.MakeText(this, toastMsg, ToastLength.Short);
                toast.Show(); //give em a message telling them it was created

                //navigate back to the main page
                var intent = new Intent(this, typeof(MainActivity));
                this.StartActivity(intent);
            }
          
            
        }

        private void TellUserToPoundSalt()
        {
            string toastMsg = string.Format("You cannot create more than {0} alerts in Free version!",maxAlertsAllowed.ToString());
            Toast toast = Toast.MakeText(this, toastMsg, ToastLength.Short);
            toast.Show();

        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.edit_alert_menu, menu);
            return base.OnCreateOptionsMenu(menu);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {

            string menuTitle = item.TitleFormatted.ToString();

            if (menuTitle.ToLower().Contains("delete"))
            {
                DeleteAlert();
            }


            return base.OnOptionsItemSelected(item);
        }
    }
}