﻿
using Android.App;
using Android.OS;
using Android.Text;
using Android.Text.Method;
using Android.Text.Util;
using Android.Views;
using Android.Widget;
using StockMyAlerts.Helpers;
using System.Text;

namespace StockMyAlerts
{
    [Activity(Label = "Stock Nanny")]
    public class SettingsActivity : Activity
    {
        private DAL dal;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            dal = new DAL();

            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.Settings);

            Switch switchPaused = FindViewById<Switch>(Resource.Id.switchPauseAlerts);
            Switch switchExtendedHours = FindViewById<Switch>(Resource.Id.switchExtendedHours);

            //set our current settings
            AppSettings currentSettings = dal.GetAppSettings();
            switchPaused.Checked = currentSettings.PauseAlerts;
            switchExtendedHours.Checked = currentSettings.MonitorExtendedHours;
            FindViewById<EditText>(Resource.Id.txtCheckInterval).Text = currentSettings.PriceCheckInterval.ToString();
            FindViewById<EditText>(Resource.Id.txtMaxAlertCount).Text = currentSettings.MaxAlertCount.ToString();
            FindViewById<EditText>(Resource.Id.txtSnoozeInterval).Text = currentSettings.SnoozeInterval.ToString();


            Button btnSave = FindViewById<Button>(Resource.Id.btnSaveSettings);
            ImageButton btnExtendedInfo = FindViewById<ImageButton>(Resource.Id.extended_hours_info);

            btnExtendedInfo.Click += delegate {
                ShowHoursInfo();
            };

            btnSave.Click += delegate {
                VerifySettings();
            };


            //set our pause switch handler
            switchPaused.CheckedChange += delegate { switchPauseAlerts_Checked(); };

            //setup our toolbar
            var toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
            SetActionBar(toolbar);
            ActionBar.Title = "Alert Settings";

            //hide our Toolbar spinner..we dont need it for this page
            Spinner alertSpinner = FindViewById<Spinner>(Resource.Id.alert_view_spinner);
            alertSpinner.Visibility = ViewStates.Gone;

        }


        private void switchPauseAlerts_Checked()
        {
            bool isChecked = FindViewById<Switch>(Resource.Id.switchPauseAlerts).Checked;
            dal.UpdatePauseAlerts(isChecked);
            if (isChecked)
            {
                //cancel all our current alarms
                AlarmHelper.StopAlarms(this);

                string toastMsg = string.Format("Alerts Paused");
                Toast toast = Toast.MakeText(this, toastMsg, ToastLength.Short);
                toast.Show();
            }
            else
            {
                AlarmHelper.StartAlarms(this);
                string toastMsg = string.Format("Alerts Active");
                Toast toast = Toast.MakeText(this, toastMsg, ToastLength.Short);
                toast.Show();
            }
        }



        private void VerifySettings()
        {
            int checkInterval = int.Parse(FindViewById<EditText>(Resource.Id.txtCheckInterval).Text);
            if (checkInterval < 60)
            {
                ShowIntervalWarning();
            }
            else
            {
                SaveSettings();
            }
        }

        private void SaveSettings()
        {
            //save our settings
            AppSettings settings = new AppSettings();
            settings.PauseAlerts = FindViewById<Switch>(Resource.Id.switchPauseAlerts).Checked;
            settings.PriceCheckInterval = int.Parse(FindViewById<EditText>(Resource.Id.txtCheckInterval).Text);
            settings.MaxAlertCount = int.Parse(FindViewById<EditText>(Resource.Id.txtMaxAlertCount).Text);
            settings.SnoozeInterval = int.Parse(FindViewById<EditText>(Resource.Id.txtSnoozeInterval).Text);
            settings.MonitorExtendedHours = FindViewById<Switch>(Resource.Id.switchExtendedHours).Checked;

            dal.UpdateAppSettings(settings);

            string toastMsg = string.Format("Settings Saved");
            Toast toast = Toast.MakeText(this, toastMsg, ToastLength.Short);
            toast.Show();
        }



        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.settings_menu, menu);
            return base.OnCreateOptionsMenu(menu);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {

            string menuTitle = item.TitleFormatted.ToString();

            if (menuTitle.ToLower().Contains("info"))
            {
                ShowInfoWindow();
            }


            return base.OnOptionsItemSelected(item);
        }

        public void ShowInfoWindow()
        {

            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            AlertDialog alert = dialog.Create();
            alert.SetTitle("App Info");
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(GetString(Resource.String.version_info));
            sb.AppendLine("\n");
            sb.AppendLine(GetString(Resource.String.support_info));
            sb.AppendLine("\n");
            sb.AppendLine(GetString(Resource.String.support_video));
            SpannableString span = new SpannableString(sb.ToString());
            Linkify.AddLinks(span, MatchOptions.All);


            alert.SetMessage(span);
            alert.Show();

            var alertTxt = (TextView)alert.FindViewById(Android.Resource.Id.Message);
            alertTxt.MovementMethod = LinkMovementMethod.Instance;

        }

        private void ShowHoursInfo()
        {
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            AlertDialog alert = dialog.Create();
            alert.SetTitle("Extended Hours Info");
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(GetString(Resource.String.hoursInfo_line1));
            sb.AppendLine(GetString(Resource.String.hoursInfo_line2));
            sb.AppendLine("");
            sb.AppendLine(GetString(Resource.String.hoursInfo_line3));
            sb.AppendLine(GetString(Resource.String.hoursInfo_line4));
            sb.AppendLine(GetString(Resource.String.hoursInfo_line5));


            alert.SetMessage(sb.ToString());
            alert.Show();
        }

        private void ShowIntervalWarning()
        {
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            AlertDialog alert = dialog.Create();
            alert.SetTitle("Battery Warning!");
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Price check interval has a dramatic effect on battery life.");
            sb.AppendLine("The recommended setting is 60 seconds.");
            alert.SetMessage(sb.ToString());

            alert.SetButton("OK", (c, ev) =>
            {
                SaveSettings();
            });


            alert.Show();

        }

    }
}