﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Support.V4.Widget;
using Android.Views;
using Android.Widget;
using Microsoft.AppCenter;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;
using StockMyAlerts.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using static Android.Widget.AdapterView;

namespace StockMyAlerts
{
    [Activity(Label = "Stock Nanny",  Icon = "@mipmap/granny_launcher")]
    public class MainActivity : Activity
    {
      
        private DAL dal;
        private List<StockAlert> lstAlerts;
        private List<StockAlert> lstEnabledAlerts;
        private List<StockAlert> lstDisabledAlerts;
        private List<StockPrice> lstStockPrices;
        private string alertsDisplayed = "enabled";
        private AppSettings appSettings;
        private bool hasNetworkIssues = false;


        public IListAdapter ListAdapater { get; private set; }

        public MainActivity()
        {
            dal = new DAL(); //init our DB access
            dal.InitAppSettings();
            appSettings = dal.GetAppSettings();
        }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            AppCenter.Start("3565b1a8-650c-4d06-9f35-68242d5b1ab0",
                   typeof(Analytics), typeof(Crashes));
            

            StartBackgroundProcess(); 
            base.OnCreate(savedInstanceState);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            //setup our toolbar and tabs
            var toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
            SetActionBar(toolbar);
            ActionBar.Title = "Alerts Overview";

            //wire up our pull to refresh on our stock list
            var swipeRefreshLayout = FindViewById<SwipeRefreshLayout>(Resource.Id.swipeRefreshLayout);
           
            swipeRefreshLayout.Refresh += delegate {
                RefreshAlertList();
            };

            PopulateAlertLists(); //generate our list of alerts
            //set our default view to Enabled
            setAlertView("enabled");

            //wire up our list click actions
            ListView lstViewAlerts = FindViewById<ListView>(Resource.Id.listViewAlerts);
            lstViewAlerts.ItemClick += LstViewAlerts_ItemClick;
          
            //set up our menu spinner for alert types
            Spinner alertSpinner = FindViewById<Spinner>(Resource.Id.alert_view_spinner);

            alertSpinner.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(alertSpinner_ItemSelected);
            var adapter = ArrayAdapter.CreateFromResource(
                    this, Resource.Array.alert_view_array,Resource.Layout.spinner_item);

            adapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
            alertSpinner.Adapter = adapter;

        }

    
        private void StartBackgroundProcess()
        {
            //if our alerts arent paused...start our background service..this should only really run on initial load
            if(!appSettings.PauseAlerts)
            {
                AlarmHelper.StartAlarms(this);
            }
         
        }

        private void RefreshAlertList()
        {
            PopulateAlertLists(); //repopulate our alert lists

            if (hasNetworkIssues)
            {
                FindViewById<SwipeRefreshLayout>(Resource.Id.swipeRefreshLayout).Refreshing = false;
                return;
            }

            if (alertsDisplayed.ToLower().Contains("enabled"))
            {
                setAlertView("enabled");
            }
            else
            {
                setAlertView("disabled");
            }


            FindViewById<SwipeRefreshLayout>(Resource.Id.swipeRefreshLayout).Refreshing = false;
        }

        private void PopulateAlertLists()
        {
            lstAlerts = dal.getAllAlerts(); //get all our alerts
            if(lstAlerts == null)
            {
                return;
            }

            //get our list of current prices
            var symbols = lstAlerts.Select(x => x.Ticker).ToList();
            try
            {
                lstStockPrices = RestData.GetStockPricesBatch(symbols);
                FindViewById<TextView>(Resource.Id.txtNetworkError).Visibility = ViewStates.Invisible;
                hasNetworkIssues = false;
            }
            catch(Exception ex)
            {
                FindViewById<TextView>(Resource.Id.txtNetworkError).Visibility = ViewStates.Visible;
                hasNetworkIssues = true;
                return;
            }

            //map our stock prices to their respective stocks
            foreach(StockAlert alert in lstAlerts)
            {
                var price = lstStockPrices.Find(x => x.Symbol == alert.Ticker).Price;
                alert.CurrentPrice = price;

            }

            //set up our separate enabled and disabled lists
            lstEnabledAlerts = lstAlerts.FindAll(x => x.AlertEnabled);
            lstDisabledAlerts = lstAlerts.FindAll(x => !x.AlertEnabled);

            
        }

        private void setAlertView(string alertType)
        {
            //make sure we dont have network issues
            if(hasNetworkIssues)
            {
                return;
            }

            ListView lstViewAlerts = FindViewById<ListView>(Resource.Id.listViewAlerts);

            if(alertType.ToLower().Contains("enabled"))
            {
                lstViewAlerts.Adapter = new StockListAdapter(this, lstEnabledAlerts); //set our custom adapter
            }
            else
            {
                lstViewAlerts.Adapter = new StockListAdapter(this, lstDisabledAlerts);
            }

            alertsDisplayed = alertType; //set our local variable so we dont have to keep rechecking it
        }

        private void alertSpinner_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            Spinner spinner = (Spinner)sender;
            string alertOption =  spinner.GetItemAtPosition(e.Position).ToString();
            setAlertView(alertOption);
        }

        private void LstViewAlerts_ItemClick(object sender,ItemClickEventArgs e)
        {
            StockAlert alert;
            if (alertsDisplayed.ToLower().Contains("enabled"))
            {
                 alert = lstEnabledAlerts[e.Position];
            }
            else
            {
                 alert = lstDisabledAlerts[e.Position];
            }
            

            var intent = new Intent(this, typeof(EditAlertActivity));
            intent.PutExtra("StockAlertId", alert.Id);
            this.StartActivity(intent);
        }

       

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.main_menu, menu);
            return base.OnCreateOptionsMenu(menu);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
           
            string menuTitle = item.TitleFormatted.ToString();

            if(menuTitle.ToLower().Contains("add"))
            {
                if (hasNetworkIssues)
                {
                    string toastMsg = string.Format("Cannot Add Alerts, Network is Disabled!");
                    Toast toast = Toast.MakeText(this, toastMsg, ToastLength.Short);
                    toast.Show();

                }
                else
                {
                    var intent = new Intent(this, typeof(EditAlertActivity));
                    intent.PutExtra("currentAlertCount", lstAlerts.Count);
                    this.StartActivity(intent);
                }
               
            }

            if (menuTitle.ToLower().Contains("settings"))
            {
                StartActivity(typeof(SettingsActivity));
            }

           
            return base.OnOptionsItemSelected(item);
        }
    }
}

